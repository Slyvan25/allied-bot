const Discord = require('discord.js');
const client = new Discord.Client();
const config = require("./config.json");
const ypi = require('youtube-channel-videos');
const request = require("request");



client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
  randomActivity();//sets first random activity from array in config
});

//#region functions
    //sets activity every 2 minutes
    function randomActivity(){
        let id = Math.floor((Math.random() * config.activities.length) + 0);
        let activity = config.activities[id];
        client.user.setActivity(activity);
    }
//#endregion

var prefix = config.prefix;
//commands
client.on('message', function(msg) {
    const args = msg.content.slice(prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    
    switch (command) {
        //#region help command
        case "help":
            msg.reply(`\r\n Hey ${msg.author} this is the list of commands u have to know!` + '\r\n' +
                "- "+ config.prefix +" r6stats {username} \r\n" +
                "- "+ config.prefix +" yt {Channel Name} - Gets the latest Youtube video from the {Channel name} \r\n" + 
                "- " + config.prefix + " insultme - insults you like those 9 year olds ingame  \r\n"
            );
            break;
        //#endregion
        
        //#region insult command
        case "insultme":
            let id = Math.floor((Math.random() * config.insults.length) + 0);
            msg.reply(`Hey ${msg.author}! ` + config.insults[id]);
            break;
        //#endregion

        //#region youtube fetch command
            case "yt":
                let [ytUrl] = args;
                request({
                    url: `https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=25&order=date&q=${ytUrl}&key=` + config.youtubeapikey,
                    json: true
                }, function (error, response, body) {
                
                    if (!error && response.statusCode === 200) {
                        try
                        {
                        let result = body.items.find((item) => item.snippet.channelTitle === ytUrl);
                        msg.reply("https://www.youtube.com/watch?v=" + result.id.videoId);
                        }
                        catch(exp){
                            msg.reply("error");
                        }
                       
                    }else{
                        msg.reply("Something went wrong!? i could not find that video or channel!");
                    }
                })

            break;         
        //#endregion

        //#region boop
        case "boop":
            let [boopTarget] = args;
            msg.channel.send(`Hey ${boopTarget}! ` + msg.author + " just booped you!");
        break;
        //#endregion

        //#region rainbowsix api fetch userdata
        case "r6":
            let [r6user] = args;
            request({
                url: `https://api.r6stats.com/api/v1/players/${r6user}/?platform=uplay`,
                json: true,
            }, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    console.log(body.player.platform);
                    let casualPlaytime = body.player.stats.casual.playtime / 60;
                    let RankedPlaytime = body.player.stats.casual.playtime / 60;

                    var embed = new Discord.RichEmbed()
                    .setAuthor("Rainbow Six Seige stats for: " + body.player.username , client.user.displayAvatarURL)
                    .addField("Ranked: ",
                    "Wins: " + body.player.stats.ranked.wins + "\r\n" +
                    "Losses: " + body.player.stats.ranked.losses + "\r\n" +
                    "Kills: " + body.player.stats.ranked.kills + "\r\n" +
                    "Deaths: " + body.player.stats.ranked.deaths + "\r\n" +
                    "kd ratio: " + body.player.stats.ranked.kd + "\r\n"
                    //"Playtime: " + Math.round(RankedPlaytime / 10) + " hours" + "\r\n"
                    ,true)

                    .addField("Casual: ",
                    "Wins: " + body.player.stats.casual.wins + "\r\n" +
                    "Losses: " + body.player.stats.casual.losses + "\r\n" +
                    "Kills: " + body.player.stats.casual.kills + "\r\n" +
                    "Deaths: " + body.player.stats.casual.deaths + "\r\n" +
                    "kd ratio: " + body.player.stats.casual.kd + "\r\n"
                    //"Playtime: " + Math.round(casualPlaytime /10) + " hours" + "\r\n"
                    ,true)

                    .addField("Overall: ",
                    "revives: " + body.player.stats.overall.revives + "\r\n" +
                    "suicides: " + body.player.stats.overall.suicides + "\r\n" +
                    "reinforcements deployed: " + body.player.stats.overall.reinforcements_deployed + "\r\n" +
                    "barricades built: " + body.player.stats.overall.barricades_built + "\r\n" +
                    "steps moved: " + body.player.stats.overall.steps_moved + "\r\n" +
                    "bullets fired: " + body.player.stats.overall.bullets_fired + "\r\n" +
                    "bullets hit: " + body.player.stats.overall.bullets_hit + "\r\n" +
                    "headshots: " + body.player.stats.overall.headshots + "\r\n" +
                    "melee kills: " + body.player.stats.overall.melee_kills + "\r\n" +
                    "penetration kills: " + body.player.stats.overall.penetration_kills + "\r\n" +
                    "assists: " + body.player.stats.overall.assists + "\r\n"
                    ,true)

                    .setFooter("Username: " + body.player.username + " Level: " + body.player.stats.progression.level + " Experience: " + body.player.stats.progression.xp);
                  msg.channel.sendEmbed(embed);
                

                }
            });

        break;

        default:
            break;
    }
});

setInterval(randomActivity, config.activiytime * 60000);//refreshes every amount of minutes that has been given in the config file
client.login(config.token);